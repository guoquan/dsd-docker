#!/bin/bash

# dsd-console
sudo docker build --tag dsdgroup/dsd-console:cpu ./dsd/cpu
sudo docker build --tag dsdgroup/dsd-console:gpu-cuda-7.0 ./dsd/gpu/cuda-7.0
sudo docker build --tag dsdgroup/dsd-console:gpu-cuda-7.5 ./dsd/gpu/cuda-7.5
sudo docker build --tag dsdgroup/dsd-console:gpu-cuda-8.0 ./dsd/gpu/cuda-8.0
sudo docker tag dsdgroup/dsd-console:gpu-cuda-7.5 dsdgroup/dsd-console:gpu
sudo docker tag dsdgroup/dsd-console:gpu-cuda-7.5 dsdgroup/dsd-console:latest

# dsd-data
sudo docker build --tag dsdgroup/dsd-data:sftp ./dsd-data/sftp
sudo docker tag dsdgroup/dsd-data:sftp dsdgroup/dsd-data:latest
sudo docker build --tag dsdgroup/dsd-data:ftp ./dsd-data/ftp

# jupyter
sudo docker build --tag dsdgroup/jupyter:cpu ./jupyter/cpu
sudo docker build --tag dsdgroup/jupyter:gpu-cuda-7.0 ./jupyter/gpu/cuda-7.0
sudo docker build --tag dsdgroup/jupyter:gpu-cuda-7.5 ./jupyter/gpu/cuda-7.5
sudo docker build --tag dsdgroup/jupyter:gpu-cuda-8.0 ./jupyter/gpu/cuda-8.0
sudo docker tag dsdgroup/jupyter:gpu-cuda-7.5 dsdgroup/jupyter:gpu
sudo docker tag dsdgroup/jupyter:cpu dsdgroup/jupyter:latest

# mxnet
sudo docker build --tag dsdgroup/mxnet:cpu ./mxnet/cpu
sudo docker build --tag dsdgroup/mxnet:gpu-cuda-7.0 ./mxnet/gpu/cuda-7.0
sudo docker build --tag dsdgroup/mxnet:gpu-cuda-7.5 ./mxnet/gpu/cuda-7.5
sudo docker build --tag dsdgroup/mxnet:gpu-cuda-8.0 ./mxnet/gpu/cuda-8.0
sudo docker tag dsdgroup/mxnet:gpu-cuda-7.5 dsdgroup/mxnet:gpu
sudo docker tag dsdgroup/mxnet:cpu dsdgroup/mxnet:latest

# theano
sudo docker build --tag dsdgroup/theano:cpu ./theano/cpu
sudo docker build --tag dsdgroup/theano:gpu-cuda-7.0 ./theano/gpu/cuda-7.0
sudo docker build --tag dsdgroup/theano:gpu-cuda-7.5 ./theano/gpu/cuda-7.5
sudo docker build --tag dsdgroup/theano:gpu-cuda-8.0 ./theano/gpu/cuda-8.0
sudo docker tag dsdgroup/theano:gpu-cuda-7.5 dsdgroup/theano:gpu
sudo docker tag dsdgroup/theano:cpu dsdgroup/theano:latest

# keras
sudo docker build --tag dsdgroup/keras:cpu ./keras/theano/cpu
sudo docker build --tag dsdgroup/keras:theano-gpu-cuda-7.0 ./keras/theano/gpu/cuda-7.0
sudo docker build --tag dsdgroup/keras:theano-gpu-cuda-7.5 ./keras/theano/gpu/cuda-7.5
sudo docker build --tag dsdgroup/keras:theano-gpu-cuda-8.0 ./keras/theano/gpu/cuda-8.0
sudo docker tag dsdgroup/keras:theano-gpu-cuda-7.5 dsdgroup/keras:theano-gpu
sudo docker tag dsdgroup/keras:cpu dsdgroup/keras:latest
