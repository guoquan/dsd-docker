#!/bin/bash

CONTAINER_NAME=dsd-data-ftp

IF="$(ip route | grep "default" | awk '{OFS=" "}{ print $5 }')"
IP="$(ip route | awk 'NR>1 && /'$IF'/ {print $9}')"
DSD_DOCKER_PATH=$(cd $(dirname $0)/..; pwd)

PORT=

args=$(getopt -o "srp:h" -l "stop,restart,port:,help" --name "$0" -- "$@")
eval set -- "$args"

while [ $# -ge 1 ]; do
    case "$1" in
        --)
            # No more options left.
            shift
            break
            ;;
        -s|--stop)
            sudo docker stop $CONTAINER_NAME && \
            sudo docker rm $CONTAINER_NAME && \
            echo "$CONTAINER_NAME has been stopped and removed."
            exit 0
            ;;
        -r|--restart)
            sudo docker restart $CONTAINER_NAME && \
            echo "$CONTAINER_NAME has been restarted."
            exit 0
            ;;
        -p|--port)
            PORT="$2"
            shift 2
            ;;
        -h|--help)
            echo -e \
                "Usage: $0 [options]\n" \
                "Options:\n" \
                " -s, --stop: stop and remove $CONTAINER_NAME container.\n" \
                " -r, --restart: restart $CONTAINER_NAME container.\n" \
                " -p, --port: specify listening port for the FTP service.\n" \
                " -h, --help: display help message.\n"
            exit 0
            ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            ;;
        :)
            echo "Option -$OPTARG requires an argument." >&2
            exit 1
            ;;
    esac
done

if [ -n "$PORT" ]; then
    PORT="$PORT:"
fi

#sudo docker run \
#    --name=$CONTAINER_NAME \
#    -d --restart=unless-stopped \
#    -e "PUBLICHOST=$IP" \
#    -p 21:21 -p 30000-30009:30000-30009 \
#    -v $DSD_DOCKER_PATH/run/volumes/data/public-0:/home/ftpusers/dsd \
#    dsdgroup/dsd-ftp

# however, passive-mode is not working

sudo docker run \
    --name=$CONTAINER_NAME \
    -d --restart=unless-stopped \
    -p ${PORT}21 \
    -v $DSD_DOCKER_PATH/run/volumes/data/public-0:/home/ftpusers/dsd \
    dsdgroup/dsd-data:ftp

echo FTP service for public data volume is started at:
echo ftp://$(sudo docker port $CONTAINER_NAME 21/tcp | sed 's/0.0.0.0/'$IP'/g')
