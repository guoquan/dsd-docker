#!/bin/bash

POLL_INTERVAL=0.1

uuid()
{
    cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w ${1:-32} | head -n 1
}

name()
{
    if [[ $2 -eq 1 ]]; then
        echo dsd-console-devel-$1
    else
        echo dsd-console-runtime
    fi
}

container_alive()
{
    [[ ! -z $(sudo docker ps -aq -f name=$1) ]]
}

wait_container()
{
    WAIT_COUNT=0
    DEC=("-" "\\" "|" "/")
    while ! $(container_alive $1); do
        printf "\r[ ${DEC[$(( $WAIT_COUNT % ${#DEC[@]} ))]} ] [ Time elapsed: $(bc <<< "$WAIT_COUNT * ${2:-1}") ] "
        ((WAIT_COUNT++))
        sleep ${2:-1}s
    done
}

get_link()
{
    IF="$(ip route | grep "default" | awk '{OFS=" "}{ print $5 }')"
    IP="$(ip route | awk 'NR>1 && /'$IF'/ {print $9}')"
    echo ${3:-http}://$(sudo docker port $1 $2 | sed 's/0.0.0.0/'$IP'/g')
}

# default arguments
DEV=0
CUDA=
PORT=80
EXT=

args=$(getopt -o "dsrc:p:e:h" -l "dev,stop,restart,cuda:,port:,extent:,help" --name "$0" -- "$@")
eval set -- "$args"

while [ $# -ge 1 ]; do
    case "$1" in
        --)
            # No more options left.
            shift
            break
            ;;
        -d|--dev)
            DEV=1
            echo "Run in development mode"
            shift
            ;;
        -s|--stop)
            #echo "Not implemented yet. Please use docker commond to stop/rm dsd-console-* container. Do it wisely." >&2
            #exit 1
            sudo docker stop dsd-console-runtime && \
            sudo docker rm dsd-console-runtime && \
            echo "dsd-console-runtime has been stopped and removed."
            exit 0
            ;;
        -r|--restart)
            sudo docker restart dsd-console-runtime && \
            echo "dsd-console-runtime has been restarted."
            exit 0
            ;;
        -c|--cuda)
            CUDA="$2"
            shift 2
            ;;
        -p|--port)
            PORT="$2"
            shift 2
            ;;
        -e|--extent)
            EXT="$2"
            shift 2
            ;;
        -h|--help)
            echo -e \
                "Usage: $0 [options]\n" \
                "Options:\n" \
                " -d, --dev: run in development mode.\n" \
                " -s, --stop: stop and remove dsd-console runtime container.\n" \
                " -r, --restart: restart dsd-console runtime container.\n" \
                " -c, --cuda: specify CUDA version.\n" \
                " -p, --port: specify listening port in runtime mode.\n" \
                " -e, --extent: extent arguments for start.sh in runtime mode container.\n" \
                " -h, --help: display help message.\n"
            exit 0
            ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            ;;
        :)
            echo "Option -$OPTARG requires an argument." >&2
            exit 1
            ;;
    esac
done

# fix arguments for flags
if [ -n "$CUDA" ]; then
    CUDA=":gpu-cuda-$CUDA"
fi
if [ -n "$PORT" ]; then
    PORT="$PORT:"
fi

# script path
DSD_PATH=$(cd $(dirname $0)/../..; pwd)
DSD_DOCKER_PATH=$(cd $(dirname $0)/..; pwd)

# ensure sudo
sudo echo hello sudo >/dev/null

NEW_UUID=$(uuid 8)
NEW_NAME=$(name $NEW_UUID $DEV)

(
wait_container $NEW_NAME $POLL_INTERVAL
sleep 1s; if $(container_alive $NEW_NAME); then
    # expose 80 (http) and 443 (https) for Nginx, and 5000 for flask, 8888 for jupyter
    echo -e \
        "\n" \
        "\n=====================================" \
        "\nContainer: $NEW_NAME" \
        "\n-------------------------------------" \
        "\nUse the following links:" \
        "\n* $(get_link $NEW_NAME 5000/tcp) for flask (DSD service)" \
        "\n* $(get_link $NEW_NAME 8888/tcp) for jupyter (DSD development)" \
        "\n=====================================" \
        "\n\n"
else
    echo "Container not start normally. Check and try again."
fi
) & (
if [[ $DEV -eq 1 ]]; then
    sudo nvidia-docker run \
        --name=$NEW_NAME \
        --rm \
        -e "DSD_DEV=$DEV" \
        -P \
        --add-host=dockerhost:$(ip route | awk '/docker0/ { print $NF }') \
        -v /var/run/docker.sock:/var/run/docker.sock \
        -v ~/.ssh:/root/.ssh:ro \
        -v $DSD_PATH:/opt/dsd:ro \
        -v $DSD_PATH/workspace:/root/workspace \
        -v $DSD_DOCKER_PATH/run/volumes:/volumes \
        -v $DSD_DOCKER_PATH/run/data:/data \
        -v $DSD_PATH:/root/workspace/dsd \
        dsdgroup/dsd-console${CUDA}
else
    if $(container_alive $NEW_NAME); then
        sudo docker start $NEW_NAME
    else
        sudo nvidia-docker run \
            --name=$NEW_NAME \
            -d --restart=unless-stopped \
            -e "DSD_DEV=$DEV" \
            -p ${PORT}5000 \
            --add-host=dockerhost:$(ip route | awk '/docker0/ { print $NF }') \
            -v /var/run/docker.sock:/var/run/docker.sock \
            -v $DSD_PATH:/opt/dsd:ro \
            -v $DSD_PATH/workspace:/root/workspace \
            -v $DSD_DOCKER_PATH/run/volumes:/volumes \
            -v $DSD_DOCKER_PATH/run/data:/data \
            dsdgroup/dsd-console${CUDA} \
            bash start.sh $EXT
    fi
fi
sleep $(bc <<< "1 + 2 * $POLL_INTERVAL")s
)
