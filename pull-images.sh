#!/bin/bash

# dsd-console
sudo docker pull --all-tags dsdgroup/dsd-console
# dsd-data
sudo docker pull --all-tags dsdgroup/dsd-data

# jupyter
sudo docker pull --all-tags dsdgroup/jupyter
# mxnet
sudo docker pull --all-tags dsdgroup/mxnet
# theano
sudo docker pull --all-tags dsdgroup/theano
# keras
sudo docker pull --all-tags dsdgroup/keras
